package com.example.day14.services;

import com.example.day14.dto.CreateUserRequest;
import com.example.day14.dto.CreateUserResponse;
import com.example.day14.dto.UpdateUserRequest;
import com.example.day14.dto.UserDetailResponse;
import com.example.day14.entities.User;
import com.example.day14.repositories.UserRepository;
import jakarta.transaction.Transactional;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service

public class UserService implements IUserService{
    @Autowired
    ModelMapper modelMapper;
    @Autowired
    UserRepository ur;
    private User convertToEntity(CreateUserRequest req) {
        return modelMapper.map(req, User.class);
    }

    private CreateUserResponse convertToDto(User user) {
        return modelMapper.map(user, CreateUserResponse.class);
    }


    @Transactional
    public CreateUserResponse createNewUser(CreateUserRequest request){
        User usr = convertToEntity(request);
        User createUser = ur.save(usr);
        return convertToDto(createUser);
    }

    @Override
    public List<UserDetailResponse> getAllUser() {
        List<User> users = ur.findAll();
        if (!users.isEmpty()) {
            return users.stream()
                    .map(user -> modelMapper.map(user, UserDetailResponse.class))
                    .collect(Collectors.toList());
        }
        return Collections.emptyList();


    }

    @Override
    public UserDetailResponse getUserId(Long id) {
        Optional<User> userOptional = ur.findById(id);
        User user = userOptional.get();
        return modelMapper.map(user, UserDetailResponse.class);
    }

    @Override
    public UserDetailResponse updateUser(UpdateUserRequest user) {
      Optional<User> userOptional = ur.findById(user.getId());
      if(userOptional.isPresent()){
            User userUpdate = userOptional.get();
            userUpdate.setName(user.getName());
            userUpdate.setEmail(user.getEmail());
            ur.save(userUpdate);
            return modelMapper.map(userUpdate, UserDetailResponse.class) ;
         }
      else {
          throw new ResponseStatusException(HttpStatus.NOT_FOUND);
      }
    }

}
