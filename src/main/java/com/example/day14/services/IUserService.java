package com.example.day14.services;

import com.example.day14.dto.CreateUserRequest;
import com.example.day14.dto.CreateUserResponse;
import com.example.day14.dto.UpdateUserRequest;
import com.example.day14.dto.UserDetailResponse;
import com.example.day14.entities.User;

import java.util.List;

public interface IUserService {
    CreateUserResponse createNewUser(CreateUserRequest request);

    List<UserDetailResponse> getAllUser();
    UserDetailResponse getUserId(Long id);
    UserDetailResponse updateUser( UpdateUserRequest user);
//
//    public User findById(Long id);
//
//    public void deleteUser(Long id);


}
