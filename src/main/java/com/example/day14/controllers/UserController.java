package com.example.day14.controllers;

import com.example.day14.dto.*;
import com.example.day14.entities.User;
import com.example.day14.repositories.UserRepository;
import com.example.day14.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/shop/v1")
public class UserController {


    @Autowired
    UserService us;

    @Autowired
    UserRepository ur;

    @PostMapping("/users")
    public ResponseEntity<CreateUserResponse> createNewUser(@RequestBody CreateUserRequest req){
        CreateUserResponse response = us.createNewUser(req);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @GetMapping("/users")
    public ResponseEntity<List<UserDetailResponse>>getAllUser(){
        List<UserDetailResponse> response = us.getAllUser();
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable("id")Long id){
        Optional<User> user = ur.findById(id);
        if(user.isPresent())
        try {
            ur.deleteById(id);
            return new ResponseEntity("Delete success", HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(ex.toString(), HttpStatus.BAD_REQUEST);
        }
    else
        return new ResponseEntity<>("User not found", HttpStatus.BAD_REQUEST);
    }
    @GetMapping("/users/{id}")
    public ResponseEntity<UserDetailResponse> getById(@PathVariable("id")Long id){
        return new  ResponseEntity<>(us.getUserId(id),HttpStatus.OK);
    }


    @PutMapping("/users")
    public ResponseEntity<UserDetailResponse> userUpdate(@RequestBody UpdateUserRequest user){
        UserDetailResponse response = us.updateUser(user);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }



}

