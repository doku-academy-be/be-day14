package com.example.day14.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class CreateUserRequest implements Serializable {
    @JsonProperty(value = "name", required = true)
    private String name;
    @JsonProperty(value = "email", required = true)
    private String email;
}
